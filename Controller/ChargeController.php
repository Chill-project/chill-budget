<?php
/*
 * 
 */
namespace Chill\AMLI\BudgetBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Chill\AMLI\BudgetBundle\Controller\AbstractElementController;
use Chill\AMLI\BudgetBundle\Entity\Charge;
use Chill\AMLI\BudgetBundle\Form\ChargeType;
use Chill\PersonBundle\Entity\Person;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ChargeController extends AbstractElementController
{
    protected function getType()
    {
        return ChargeType::class;
    }
    
    protected function createNewElement()
    {
        return new Charge();
    }
    
    /**
     * 
     * @Route(
     *  "{_locale}/budget/charge/{id}/view",
     *  name="chill_budget_charge_view"
     * )
     * @param Charge $charge
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Charge $charge)
    {
        return $this->_view($charge, '@ChillAMLIBudget/Charge/view.html.twig');
    }
    
    /**
     * @Route(
     *  "{_locale}/budget/charge/by-person/{id}/new",
     *  name="chill_budget_charge_new"
     * )
     * 
     * @param Request $request
     * @param Person $person
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Person $person)
    {
        return $this->_new(
            $person, 
            $request, 
            '@ChillAMLIBudget/Charge/new.html.twig', 
            'Charge created');
    }
    
    /**
     * @Route(
     *  "{_locale}/budget/charge/{id}/edit",
     *  name="chill_budget_charge_edit"
     * )
     * 
     * @param Request $request
     * @param Charge $charge
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Charge $charge)
    {
        return $this->_edit(
            $charge, 
            $request, 
            '@ChillAMLIBudget/Charge/edit.html.twig', 
            'Charge updated');
    }
    
    /**
     * 
     * @Route(
     *  "{_locale}/budget/charge/{id}/delete",
     *  name="chill_budget_charge_delete"
     * )
     * 
     * @param Request $request
     * @param Charge $charge
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, Charge $charge)
    {
        return $this->_delete(
            $charge, 
            $request, 
            '@ChillAMLIBudget/Charge/confirm_delete.html.twig', 
            'Charge deleted');
    }
}
