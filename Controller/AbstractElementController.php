<?php

namespace Chill\AMLI\BudgetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Chill\AMLI\BudgetBundle\Entity\AbstractElement;
use Chill\AMLI\BudgetBundle\Security\Authorization\BudgetElementVoter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Translation\TranslatorInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractElementController extends Controller
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $chillMainLogger;
    
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        LoggerInterface $chillMainLogger
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->chillMainLogger = $chillMainLogger;
    }

    /**
     * @return AbstractElement the newly created element
     */
    abstract protected function createNewElement();
    
    abstract protected function getType();

    /**
     * 
     */
    protected function _new(Person $person, Request $request, $template, $flashMessageOnSuccess)
    {
        /* @var $element \Chill\AMLI\BudgetBundle\Entity\AbstractElement */
        $element = $this->createNewElement()
            ->setPerson($person)
            ;
        
        $this->denyAccessUnlessGranted(BudgetElementVoter::CREATE, $element);
        
        $form = $this->createForm($this->getType(), $element);
        $form->add('submit', SubmitType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($element);
            $em->flush();
            
            $this->addFlash('success', $this->translator->trans($flashMessageOnSuccess));
            
            return $this->redirectToRoute('chill_budget_elements_index', [
                'id' => $person->getId()
            ]);
        } elseif ($form->isSubmitted()) {
            $this->addFlash('error', $this->translator->trans('This form contains errors'));
        }
        
        return $this->render($template, array(
            'form' => $form->createView(),
            'person' => $person,
            'element' => $element
        ));
    }

    /**
     * 
     * @param AbstractElement $element
     * @param Request $request
     * @param string $template
     * @param string $flashOnSuccess
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function _edit(AbstractElement $element, Request $request, $template, $flashOnSuccess)
    {
        $this->denyAccessUnlessGranted(BudgetElementVoter::UPDATE, $element);
        
        $form = $this->createForm($this->getType(), $element);
        $form->add('submit', SubmitType::class);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            
            $this->addFlash('success', $this->translator->trans($flashOnSuccess));
            
            return $this->redirectToRoute('chill_budget_elements_index', [
                'id' => $element->getPerson()->getId()
            ]);
        }
        
        return $this->render($template, array(
            'element' => $element,
            'form' => $form->createView(),
            'person' => $element->getPerson()
        ));
    }
    
    /**
     * 
     * Route(
     *  "{_locale}/family-members/family-members/{id}/delete",
     *  name="chill_family_members_family_members_delete"
     * )
     * 
     * @param AbstractElement $element
     * @param Request $request
     * @return \Symfony\Component\BrowserKit\Response
     */
    protected function _delete(AbstractElement $element, Request $request, $template, $flashMessage)
    {
        $this->denyAccessUnlessGranted(BudgetElementVoter::DELETE, $element, 'You are not '
             . 'allowed to delete this family membership');

        $form = $this->createDeleteForm();

        if ($request->getMethod() === Request::METHOD_DELETE) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->chillMainLogger->notice("A budget element has been removed", array(
                   'family_element' => get_class($element),
                   'by_user' => $this->getUser()->getUsername(),
                   'family_member_id' => $element->getId(),
                   'amount' => $element->getAmount(),
                   'type' => $element->getType()
                ));

                $em = $this->getDoctrine()->getManager();
                $em->remove($element);
                $em->flush();

                $this->addFlash('success', $this->translator
                        ->trans($flashMessage));

                return $this->redirectToRoute('chill_budget_elements_index', array(
                     'id' => $element->getPerson()->getId()
                  ));
            }
        }


        return $this->render($template, array(
           'element' => $element,
           'delete_form' => $form->createView()
        ));
    }
    
    /**
     * Route(
     *  "{_locale}/family-members/family-members/{id}/view",
     *  name="chill_family_members_family_members_view"
     * )
     */
    protected function _view(AbstractElement $element, $template)
    {
        $this->denyAccessUnlessGranted(BudgetElementVoter::SHOW, $element);
        
        return $this->render($template, array(
            'element' => $element
        ));
    }
    
    /**
     * Creates a form to delete a help request entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm()
    {
        return $this->createFormBuilder()
            ->setMethod(Request::METHOD_DELETE)
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }

}
