<?php

namespace Chill\AMLI\BudgetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Translation\TranslatorInterface;
use Psr\Log\LoggerInterface;
use Chill\AMLI\BudgetBundle\Entity\Charge;
use Chill\AMLI\BudgetBundle\Entity\Resource;
use Chill\AMLI\BudgetBundle\Security\Authorization\BudgetElementVoter;
use Chill\AMLI\BudgetBundle\Calculator\CalculatorManager;

class ElementController extends Controller
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $chillMainLogger;
    
    /**
     *
     * @var CalculatorManager
     */
    protected $calculator;
    
    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        LoggerInterface $chillMainLogger,
        CalculatorManager $calculator
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->chillMainLogger = $chillMainLogger;
        $this->calculator = $calculator;
    }
    
    /**
     * @Route(
     *  "{_locale}/budget/elements/by-person/{id}",
     *  name="chill_budget_elements_index"
     * )
     */
    public function indexAction(Person $person)
    {
        $this->denyAccessUnlessGranted(BudgetElementVoter::SHOW, $person);
        
        $charges = $this->em
            ->getRepository(Charge::class)
            ->findByPerson($person);
        $ressources = $this->em
            ->getRepository(Resource::class)
            ->findByPerson($person);
        
        $now = new \DateTime('now');
        
        $actualCharges = $this->em
            ->getRepository(Charge::class)
            ->findByPersonAndDate($person, $now);
        $actualResources = $this->em
            ->getRepository(Resource::class)
            ->findByPersonAndDate($person, $now);
        
        $elements = \array_merge($actualCharges, $actualResources);
        
        if (count($elements) > 0) {
            $results = $this->calculator->calculateDefault($elements);
        }
        
        return $this->render('ChillAMLIBudgetBundle:Element:index.html.twig', array(
            'person' => $person,
            'charges' => $charges,
            'resources' => $ressources,
            'results' => $results ?? []
        ));
    }

}
