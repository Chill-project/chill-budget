<?php
/*
 * 
 */
namespace Chill\AMLI\BudgetBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Chill\AMLI\BudgetBundle\Controller\AbstractElementController;
use Chill\AMLI\BudgetBundle\Entity\Resource;
use Chill\AMLI\BudgetBundle\Form\ResourceType;
use Chill\PersonBundle\Entity\Person;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ResourceController extends AbstractElementController
{

    protected function getType()
    {
        return ResourceType::class;
    }
    
    protected function createNewElement()
    {
        return new Resource();
    }
    
    /**
     * 
     * @Route(
     *  "{_locale}/budget/resource/{id}/view",
     *  name="chill_budget_resource_view"
     * )
     * @param Request $request
     * @param Resource $resource
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Resource $resource)
    {
        return $this->_view($resource, '@ChillAMLIBudget/Resource/view.html.twig');
    }
    
    /**
     * @Route(
     *  "{_locale}/budget/resource/by-person/{id}/new",
     *  name="chill_budget_resource_new"
     * )
     * 
     * @param Request $request
     * @param Person $person
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Person $person)
    {
        return $this->_new(
            $person, 
            $request, 
            '@ChillAMLIBudget/Resource/new.html.twig', 
            'Resource created');
    }
    
    /**
     * @Route(
     *  "{_locale}/budget/resource/{id}/edit",
     *  name="chill_budget_resource_edit"
     * )
     * 
     * @param Request $request
     * @param Resource $resource
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Resource $resource)
    {
        return $this->_edit(
            $resource, 
            $request, 
            '@ChillAMLIBudget/Resource/edit.html.twig', 
            'Resource updated');
    }
    
    /**
     * 
     * @Route(
     *  "{_locale}/budget/resource/{id}/delete",
     *  name="chill_budget_resource_delete"
     * )
     * 
     * @param Request $request
     * @param Resource $resource
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, Resource $resource)
    {
        return $this->_delete($resource, 
            $request, 
            '@ChillAMLIBudget/Resource/confirm_delete.html.twig', 
            'Resource deleted');
    }
}
