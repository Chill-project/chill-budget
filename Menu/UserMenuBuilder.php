<?php
/*
 */
namespace Chill\AMLI\BudgetBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Chill\AMLI\BudgetBundle\Security\Authorization\BudgetElementVoter;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     *
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker, 
        TranslatorInterface $translator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    
    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $parameters['person'];
        
        if ($this->authorizationChecker->isGranted(BudgetElementVoter::SHOW, $person)) {
            $menu->addChild(
                $this->translator->trans('Budget'), [
                    'route' => 'chill_budget_elements_index',
                    'routeParameters' => [ 'id' => $person->getId() ],
                ])
                ->setExtra('order', 460)
                ;
        }
    }

    public static function getMenuIds(): array
    {
        return [ 'person' ];
    }
}
