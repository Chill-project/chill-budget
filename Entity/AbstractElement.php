<?php

namespace Chill\AMLI\BudgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AbstractElement
 *
 * @ORM\MappedSuperclass()
 */
abstract class AbstractElement
{
    
    /**
     *
     * @var Person
     * @ORM\ManyToOne(
     *  targetEntity="\Chill\PersonBundle\Entity\Person"
     * )
     */
    private $person;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var decimal
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2)
     * @Assert\GreaterThan(
     *  value=0
     * )
     * @Assert\NotNull(
     *  message="The amount cannot be empty"
     * )
     * 
     */
    private $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(name="startDate", type="datetime_immutable")
     * @Assert\Date()
     */
    private $startDate;

    /**
     * @var \DateTimeImmutable|null
     *
     * @ORM\Column(name="endDate", type="datetime_immutable", nullable=true)
     * @Assert\GreaterThan(
     *   propertyPath="startDate",
     *   message="The budget element's end date must be after the start date"
     * )
     */
    private $endDate;
    
    abstract public function isCharge(): bool;
    
    abstract public function isResource(): bool;
    
    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person)
    {
        $this->person = $person;
        
        return $this;
    }
    
    /**
     * Set type.
     *
     * @param string $type
     *
     * @return AbstractElement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount.
     *
     * @param string $amount
     *
     * @return AbstractElement
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return double
     */
    public function getAmount()
    {
        return (double) $this->amount;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return AbstractElement
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set startDate.
     *
     * @param \DateTimeInterface  $startDate
     *
     * @return AbstractElement
     */
    public function setStartDate(\DateTimeInterface $startDate)
    {
        if ($startDate instanceof \DateTime) {
            $this->startDate = \DateTimeImmutable::createFromMutable($startDate);
        } elseif (NULL === $startDate) {
            $this->startDate = null;
        } else {
            $this->startDate = $startDate;
        }

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTimeImmutable
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTimeInterface|null $endDate
     *
     * @return AbstractElement
     */
    public function setEndDate(\DateTimeInterface $endDate = null)
    {
        if ($endDate instanceof \DateTime) {
            $this->endDate = \DateTimeImmutable::createFromMutable($endDate);
        } elseif (NULL === $endDate) {
            $this->endDate = null;
        } else {
            $this->endDate = $endDate;
        }

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTimeImmutable|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
    
    public function isEmpty()
    {
        return $this->amount == 0;
    }
}
