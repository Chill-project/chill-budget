<?php

namespace Chill\AMLI\BudgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Chill\MainBundle\Entity\HasCenterInterface;

/**
 * Charge
 *
 * @ORM\Table(name="chill_budget.charge")
 * @ORM\Entity(repositoryClass="Chill\AMLI\BudgetBundle\Repository\ChargeRepository")
 */
class Charge extends AbstractElement implements HasCenterInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string
     * @ORM\Column(name="help", type="string", nullable=true)
     */
    private $help = self::HELP_NOT_RELEVANT;
    
    const HELP_ASKED = 'running';
    const HELP_NO = 'no';
    const HELP_YES = 'yes';
    const HELP_NOT_RELEVANT = 'not-relevant';
    
    const HELPS = [
        self::HELP_ASKED,
        self::HELP_NO,
        self::HELP_YES,
        self::HELP_NOT_RELEVANT
    ];
    
        
    public function __construct()
    {
        $this->setStartDate(new \DateTimeImmutable('today'));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getHelp()
    {
        return $this->help;
    }

    public function setHelp($help)
    {
        $this->help = $help;
        
        return $this;
    }

    public function getCenter(): \Chill\MainBundle\Entity\Center
    {
        return $this->getPerson()->getCenter();
    }

    public function isCharge(): bool
    {
        return true;
    }

    public function isResource(): bool
    {
        return false;
    }
}
