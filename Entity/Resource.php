<?php

namespace Chill\AMLI\BudgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Chill\MainBundle\Entity\HasCenterInterface;

/**
 * Resource
 *
 * @ORM\Table(name="chill_budget.resource")
 * @ORM\Entity(repositoryClass="Chill\AMLI\BudgetBundle\Repository\ResourceRepository")
 */
class Resource extends AbstractElement implements HasCenterInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    public function __construct()
    {
        $this->setStartDate(new \DateTimeImmutable('today'));
    }

    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCenter(): \Chill\MainBundle\Entity\Center
    {
        return $this->getPerson()->getCenter();
    }

    public function isCharge(): bool
    {
        return false;
    }

    public function isResource(): bool
    {
        return true;
    }
}
