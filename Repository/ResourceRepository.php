<?php

namespace Chill\AMLI\BudgetBundle\Repository;

use Chill\PersonBundle\Entity\Person;

/**
 * ResourceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ResourceRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByPersonAndDate(Person $person, \DateTime $date, $sort = null)
    {
        $qb = $this->createQueryBuilder('c');
        
        $qb->where('c.person = :person')
            ->andWhere('c.startDate < :date')
            ->andWhere('c.startDate < :date OR c.startDate IS NULL')
            ;
        
        if ($sort !== null) {
            $qb->orderBy($sort);
        }
        
        $qb->setParameters([
            'person' => $person,
            'date' => $date
        ]);
        
        return $qb->getQuery()->getResult();
    }
}
