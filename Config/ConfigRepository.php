<?php
/*
 */
namespace Chill\AMLI\BudgetBundle\Config;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ConfigRepository
{
    /**
     *
     * @var array
     */
    protected $resources;
    
    /**
     *
     * @var array
     */
    protected $charges;
    
    public function __construct($resources, $charges)
    {
        $this->resources = $resources;
        $this->charges = $charges;
    }
    
    /**
     * 
     * @return array where keys are the resource'key and label the ressource label
     */
    public function getResourcesLabels()
    {
        $resources = array();
        
        foreach ($this->resources as $definition) {
            $resources[$definition['key']] = $this->normalizeLabel($definition['labels']);
        }
        
        return $resources;
    }
    
    /**
     * 
     * @return array where keys are the resource'key and label the ressource label
     */
    public function getChargesLabels()
    {
        $charges = array();
        
        foreach ($this->charges as $definition) {
            $charges[$definition['key']] = $this->normalizeLabel($definition['labels']);
        }
        
        return $charges;
    }
    
    private function normalizeLabel($labels) 
    {
        $normalizedLabels = array();
        
        foreach ($labels as $labelDefinition) {
            $normalizedLabels[$labelDefinition['lang']] = $labelDefinition['label'];
        }
        
        return $normalizedLabels;
    }

}
