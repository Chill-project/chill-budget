<?php

namespace Chill\AMLI\BudgetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\AMLI\BudgetBundle\Config\ConfigRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\AMLI\BudgetBundle\Entity\Charge;

class ChargeType extends AbstractType
{
   /**
     *
     * @var ConfigRepository
     */
    protected $configRepository;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(
        ConfigRepository $configRepository, 
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->configRepository = $configRepository;
        $this->translatableStringHelper = $translatableStringHelper;
    }

    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => $this->getTypes(),
                'placeholder' => 'Choose a charge type'
            ])
            ->add('amount', MoneyType::class)
            ->add('comment', TextAreaType::class, [
                'required' => false
            ])  
            ;
        
        if ($options['show_start_date']) {
            $builder->add('startDate', ChillDateType::class, [
                'label' => 'Start of validity period'
            ]);
        }
        
        if ($options['show_end_date']) {
            $builder->add('endDate', ChillDateType::class, [
                'required' => false,
                'label' => 'End of validity period'
            ]);
        }
        
        if ($options['show_help']) {
            $builder->add('help', ChoiceType::class, [
                'choices' => [
                    'charge.help.running' => Charge::HELP_ASKED,
                    'charge.help.no' => Charge::HELP_NO,
                    'charge.help.yes' => Charge::HELP_YES,
                    'charge.help.not-concerned' => Charge::HELP_NOT_RELEVANT
                ],
                'placeholder' => 'Choose a status',
                'required' => false,
                'label' => 'Help to pay charges'
            ]);
        }
    }
    
    private function getTypes()
    {
        $charges = $this->configRepository
            ->getChargesLabels();
        
        // rewrite labels to filter in language
        foreach ($charges as $key => $labels) {
            $charges[$key] = $this->translatableStringHelper->localize($labels);
        }
        
        \asort($charges);
        
        return \array_flip($charges);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\AMLI\BudgetBundle\Entity\Charge',
            'show_start_date' => true,
            'show_end_date' => true,
            'show_help' => true
        ));
        
        $resolver
            ->setAllowedTypes('show_start_date', 'boolean')
            ->setAllowedTypes('show_end_date', 'boolean')
            ->setAllowedTypes('show_help', 'boolean')
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'chill_amli_budgetbundle_charge';
    }


}
