<?php

namespace Chill\AMLI\BudgetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\AMLI\BudgetBundle\Entity\Resource;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\AMLI\BudgetBundle\Config\ConfigRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ResourceType extends AbstractType
{
    /**
     *
     * @var ConfigRepository
     */
    protected $configRepository;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(
        ConfigRepository $configRepository, 
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->configRepository = $configRepository;
        $this->translatableStringHelper = $translatableStringHelper;
    }

    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => $this->getTypes(),
                'placeholder' => 'Choose a resource type',
                'label' => 'Resource element type'
            ])
            ->add('amount', MoneyType::class)
            ->add('comment', TextAreaType::class, [
                'required' => false
            ])  
            ;
        
        if ($options['show_start_date']) {
            $builder->add('startDate', ChillDateType::class, [
                'label' => 'Start of validity period'
            ]);
        }
        
        if ($options['show_end_date']) {
            $builder->add('endDate', ChillDateType::class, [
                'required' => false,
                'label' => 'End of validity period'
            ]);
        }
    }
    
    private function getTypes()
    {
        $resources = $this->configRepository
            ->getResourcesLabels();
        
        // rewrite labels to filter in language
        foreach ($resources as $key => $labels) {
            $resources[$key] = $this->translatableStringHelper->localize($labels);
        }
        
        asort($resources);
        
        return \array_flip($resources);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Resource::class,
            'show_start_date' => true,
            'show_end_date' => true
        ));
        
        $resolver
            ->setAllowedTypes('show_start_date', 'boolean')
            ->setAllowedTypes('show_end_date', 'boolean')
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'chill_amli_budgetbundle_resource';
    }


}
