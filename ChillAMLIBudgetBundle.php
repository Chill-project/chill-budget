<?php

namespace Chill\AMLI\BudgetBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Chill\AMLI\BudgetBundle\DependencyInjection\Compiler\CalculatorCompilerPass;

class ChillAMLIBudgetBundle extends Bundle
{
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
        parent::build($container);
        
        $container->addCompilerPass(new CalculatorCompilerPass());
    }
}
