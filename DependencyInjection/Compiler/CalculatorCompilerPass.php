<?php
/*
 * 
 */
namespace Chill\AMLI\BudgetBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CalculatorCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $manager = $container->getDefinition('Chill\AMLI\BudgetBundle\Calculator\CalculatorManager');
        
        foreach ($container->findTaggedServiceIds('chill_budget.calculator') as $id => $tags) {
            foreach($tags as $tag) {
                $reference = new Reference($id);

                $manager->addMethodCall('addCalculator', [ $reference, $tag['default'] ?? false ]);
            }
        }
    }
}
