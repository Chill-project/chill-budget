<?php

namespace Chill\AMLI\BudgetBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('chill_amli_budget');
        
        $rootNode
            ->children()
            
                // ressources
                ->arrayNode('resources')->isRequired()->requiresAtLeastOneElement()
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('key')->isRequired()->cannotBeEmpty()
                                ->info('the key stored in database')
                                ->example('salary')
                            ->end()
                            ->arrayNode('labels')->isRequired()->requiresAtLeastOneElement()
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('lang')->isRequired()->cannotBeEmpty()
                                            ->example('fr')
                                        ->end()
                                        ->scalarNode('label')->isRequired()->cannotBeEmpty()
                                            ->example('Salaire')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            
                ->arrayNode('charges')->isRequired()->requiresAtLeastOneElement()
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('key')->isRequired()->cannotBeEmpty()
                                ->info('the key stored in database')
                                ->example('salary')
                            ->end()
                            ->arrayNode('labels')->isRequired()->requiresAtLeastOneElement()
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('lang')->isRequired()->cannotBeEmpty()
                                            ->example('fr')
                                        ->end()
                                        ->scalarNode('label')->isRequired()->cannotBeEmpty()
                                            ->example('Salaire')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            
            ->end()
            ;

        return $treeBuilder;
    }
}
