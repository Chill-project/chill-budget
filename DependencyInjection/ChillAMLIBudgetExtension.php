<?php

namespace Chill\AMLI\BudgetBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\AMLI\BudgetBundle\Security\Authorization\BudgetElementVoter;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChillAMLIBudgetExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services/config.yml');
        $loader->load('services/form.yml');
        $loader->load('services/security.yml');
        $loader->load('services/controller.yml');
        $loader->load('services/templating.yml');
        $loader->load('services/menu.yml');
        $loader->load('services/calculator.yml');
        
        $this->storeConfig('resources', $config, $container);
        $this->storeConfig('charges', $config, $container);
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependAuthorization($container);
        $this->prependRoutes($container);
    }
    
    protected function storeConfig($position, array $config, ContainerBuilder $container) 
    {
        $container
            ->setParameter(sprintf('chill_budget.%s', $position), $config[$position])
            ;
    }

    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
           'role_hierarchy' => array(
               BudgetElementVoter::UPDATE => [ BudgetElementVoter::SHOW ],
               BudgetElementVoter::CREATE => [ BudgetElementVoter::SHOW ]
           )
        ));
    }
    
    /* (non-PHPdoc)
     * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
     */
    public function prependRoutes(ContainerBuilder $container) 
    {
        //add routes for custom bundle
         $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillAMLIBudgetBundle/Resources/config/routing.yml'
              )
           )
        ));
    }
}
