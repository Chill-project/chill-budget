<?php
/*
 * 
 */
namespace Chill\AMLI\BudgetBundle\Templating;

use Twig\Extension\AbstractExtension;
use Chill\AMLI\BudgetBundle\Config\ConfigRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class Twig extends AbstractExtension
{
    /**
     *
     * @var ConfigRepository
     */
    protected $configRepository;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(
        ConfigRepository $configRepository, 
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->configRepository = $configRepository;
        $this->translatableStringHelper = $translatableStringHelper;
    }

    
    public function getFilters()
    {
        return [
            new \Twig_Filter('budget_element_type_display', [ $this, 'displayLink' ], [ 'is_safe' => [ 'html' ]])
        ];
    }
    
    public function displayLink($link, $family)
    {
        switch($family) {
            case 'resource':
                return $this->translatableStringHelper->localize(
                    $this->configRepository->getResourcesLabels()[$link]
                    );
            case 'charge':
                return $this->translatableStringHelper->localize(
                    $this->configRepository->getChargesLabels()[$link]
                    );
            default:
                throw new \UnexpectedValueException("This family of element: $family is not "
                    . "supported. Supported families are 'resource', 'charge'");
        }

    }

}
