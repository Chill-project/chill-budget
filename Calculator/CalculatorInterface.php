<?php
/*
 * 
 */
namespace Chill\AMLI\BudgetBundle\Calculator;

use Chill\AMLI\BudgetBundle\Entity\AbstractElement;

/**
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface CalculatorInterface
{
    /**
     * 
     * @param AbstractElement[] $elements
     */
    public function calculate(array $elements) : ?CalculatorResult;
    
    public function getAlias();
}
