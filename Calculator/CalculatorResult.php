<?php
/*
 * 
 */
namespace Chill\AMLI\BudgetBundle\Calculator;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CalculatorResult
{
    const TYPE_RATE = 'rate';
    const TYPE_CURRENCY = 'currency';
    const TYPE_PERCENTAGE = 'percentage';
    
    public $type;
    
    public $result;
    
    public $label;
}
