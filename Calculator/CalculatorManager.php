<?php
/*
 * 
 */
namespace Chill\AMLI\BudgetBundle\Calculator;

use Chill\AMLI\BudgetBundle\Entity\AbstractElement;
/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CalculatorManager
{
    /**
     *
     * @var CalculatorInterface[]
     */
    protected $calculators = [];
    
    protected $defaultCalculator = [];
    
    public function addCalculator(CalculatorInterface $calculator, bool $default)
    {
        $this->calculators[$calculator::getAlias()] = $calculator;
        
        if ($default) {
            $this->defaultCalculator[] = $calculator::getAlias();
        }
    }
    
    /**
     * 
     * @param string $alias
     * @return CalculatorInterface
     */
    public function getCalculator($alias)
    {
        if (FALSE === \array_key_exists($alias, $this->calculators)) {
            throw new \OutOfBoundsException("The calculator with alias '$alias' does "
                . "not exists. Possible values are ". \implode(", ", \array_keys($this->calculators)));
        }
        
        return $this->calculators[$alias];
    }
    
    /**
     * 
     * @param AbstractElement[] $elements
     * @return CalculatorResult[]
     */
    public function calculateDefault(array $elements)
    {
        $results = [];
        
        foreach ($this->defaultCalculator as $alias) {
            $calculator = $this->calculators[$alias];
            $result = $calculator->calculate($elements);
            
            if ($result !== null) {
                $results[$calculator::getAlias()] = $result;
            }
        }
        
        return $results;
    }
}
